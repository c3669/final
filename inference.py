import cv2
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import yaml
from top_field.inference import Perspective2D
import torch
import os
from yolo_inference.detect import activate
from field_selection.run import test_model
import pandas as pd
import glob
from IPython.display import clear_output


def preprocess_filed_and_detectoin():
    test_model()  # выделение поля
    txt_path = activate()  # обнаружение объектов
    return txt_path


def clear():
    # Если будем хранить игроков, то оставить кропы
    file_format = ['txt', 'jpg', 'png']
    for j in file_format:
        files = glob.glob(f'runs/detect/kek/**/**/*.{j}', recursive=True)
        for i in files:
            try:
                os.remove(i)
            except OSError as e:
                print("Error: %s : %s" % (i, e.strerror))


def convert_box(box: list, image_shape: tuple):
    # box[0] - center box by X
    # box[1] - center box by Y
    # box[2] - width
    # box[3] - height

    x_min = box[0] * image_shape[0] - box[2] * image_shape[0] / 2
    y_min = box[1] * image_shape[1] - box[3] * image_shape[1] / 2
    width = box[2] * image_shape[0]
    height = box[3] * image_shape[1]

    return [x_min, y_min, width, height]


def coordinate_to_scheme_field(optim_homography, path_txt) -> tuple:
    labels, boxs = parse_file(os.path.join(path_txt))
    x_final_person, y_final_person = list(), list()
    x_final_ball, y_final_ball = list(), list()

    for idx, label in enumerate(labels):
        r = 10

        if boxs[idx][0] + boxs[idx][2] >= 1280:
            x = 1280 - r
        else:
            x = boxs[idx][0] + boxs[idx][2] / 2

        if boxs[idx][1] + boxs[idx][3] >= 720:
            y = 720 - r
        else:
            y = boxs[idx][1] + boxs[idx][3]
        x = torch.tensor(x / 1280 - 0.5).float()
        y = torch.tensor(y / 720 - 0.5).float()
        xy = torch.stack([x, y, torch.ones_like(x)])

        xy_warped = torch.matmul(torch.tensor(optim_homography), xy)

        xy_warped, z_warped = xy_warped.split(2, dim=1)
        xy_warped = 2.0 * xy_warped / (z_warped + 1e-8)
        x_warped, y_warped = torch.unbind(xy_warped, dim=1)

        x_warped = (x_warped.item() * 0.5 + 0.5) * 1050
        y_warped = (y_warped.item() * 0.5 + 0.5) * 680

        if x_warped > 1050:
            x_warped = 1045
        elif x_warped < 0:
            x_warped = 5
        if y_warped > 680:
            y_warped = 675
        elif y_warped < 0:
            y_warped = 5

        if label == 0:
            x_final_person.append(x_warped)
            y_final_person.append(y_warped)
        elif label == 32:
            x_final_ball.append(x_warped)
            y_final_ball.append(y_warped)

    return x_final_person, y_final_person, x_final_ball, y_final_ball, labels  #


def save_object(dataset: pd, frame_id: int, x_coords: list, y_coords: list, cls: list, num_obj: int = None) -> None:
    curr_dataset = pd.DataFrame(columns=['frame_id', 'obj_id', 'x_coord', 'y_coord', 'class'])
    curr_dataset['frame_id'] = [frame_id for _ in range(len(x_coords))]
    curr_dataset['obj_id'] = [idx + 1 for idx in range(len(x_coords))]
    curr_dataset['x_coord'] = np.array(x_coords) / 10
    curr_dataset['y_coord'] = np.array(y_coords) / 10
    curr_dataset['class'] = cls
    dataset = pd.concat([dataset, curr_dataset], ignore_index=True)
    return dataset


def magic(video_path, fs_directory, txt_directory):
    vidcap = cv2.VideoCapture(video_path)
    counter = 0
    df = pd.DataFrame(columns=['frame_id', 'obj_id', 'x_coord', 'y_coord', 'class'])
    perspective = Perspective2D(test=False)
    while True:
        success, image = vidcap.read()
        if not success:
            break

        cv2.imwrite(f'./results/frame/orig_{counter}.jpg', image)
        cv2.imwrite(fs_directory + 'frame' + str(counter) + '.jpg', image)  # save frame as JPEG file
        txt_path = preprocess_filed_and_detectoin()

        pil_image = Image.fromarray(np.uint8(image[..., ::-1]))
        pil_image = pil_image.resize([1280, 720], resample=Image.NEAREST)
        image_ = np.array(pil_image)
        warmap_main, warmap, optim_homography = perspective.warmaped(image_)

        path_txt = os.path.join(txt_directory, f'frame{counter}_fake_C.txt')

        x_person, y_person, x_ball, y_ball, labels = coordinate_to_scheme_field(optim_homography, path_txt)

        os.remove(fs_directory + 'frame' + str(counter) + '.jpg')
        os.remove('./field_selection/results/fake_C/' + 'frame' + str(counter) + '_fake_C' + '.png')

        plt.imshow(warmap)
        plt.scatter(x_person, y_person, c='blue')
        plt.scatter(x_ball, y_ball, c='red')
        plt.savefig(f'./results/warmap/warmap_{counter}.png')
        plt.close()

        plt.imshow(warmap_main)
        plt.scatter(x_person, y_person, c='blue')
        plt.scatter(x_ball, y_ball, c='red')
        plt.savefig(f'results/warmap_with_pos_camera/warmap_{counter}.png')
        plt.close()

        x_final = x_person + x_ball
        y_final = y_person + y_ball
        df = save_object(df, counter, x_final, y_final, labels)

        counter += 1
        clear_output()

        if counter > 50:
            break

    df.to_csv('./results/coords.csv', index=False)


def parse_file(file, image_shape: tuple = (1280, 592)):
    with open(file, 'r') as f:
        coord = [values.split() for values in f.readlines()]

    labels = [int(value[0]) for value in coord]
    boxs = [np.array(value[1:], dtype=float) for value in coord]

    boxs = [convert_box(box, image_shape) for box in boxs]
    return labels, boxs


def run():
    with open('./parametrs.yaml') as file:
        parameters = yaml.safe_load(file)

    video_path = parameters['video_path']
    fs_directory = parameters['fs_directory']
    txt_directory = parameters['txt_directory']
    clear_dir = parameters['clear_dir']

    magic(video_path, fs_directory, txt_directory)

    if clear_dir:
        clear()


if __name__ == '__main__':
    run()
